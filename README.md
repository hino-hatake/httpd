## A. Intro
#### 1. Versions in use
| Component | Version  |
| ---       | ---      |
| docker    | 20.10    |
| compose   | 1.28     |
| apache    | 2.4      |

## B. How to use
#### 1. Compose commands
Up:
```bash
docker-compose up -d && docker-compose logs -f
```
Recreate:
```bash
docker-compose up -d --force-recreate && docker-compose logs -f --tail 1
```
Copy files form container:
```bash
docker cp httpd:/usr/local/apache2/conf/httpd.conf .
docker cp httpd:/usr/local/apache2/htdocs/index.html .
```

#### 2. Basic Auth
Create password file:
```bash
htpasswd -cb /opt/apache2/users hino password
```
Or you may want to run with `docker-compose`:
```bash
docker-compose exec httpd htpasswd -cb /opt/apache2/users hino password
```
Test your password:
```bash
htpasswd -vb /opt/apache2/users hino password
docker-compose exec httpd htpasswd -vb /opt/apache2/users hino password
```
Update/add your `Directory` block:
```bash
<Directory "/usr/local/apache/htdocs/pandora">
  ...
  AuthType Basic
  AuthName Pandora
  AuthBasicProvider file
  AuthUserFile "/opt/apache2/users"
  Require valid-user
  ...

  # Require all granted
  ...
</Directory>
```
Try to open the pandora's box:
```bash
curl -sIXGET 127.0.0.1/pandora
HTTP/1.1 401 Unauthorized
WWW-Authenticate: Basic realm="Pandora"
```
Another try:
```bash
curl -sIXGET 127.0.0.1/pandora/ -u hino:password
HTTP/1.1 200 OK
```
Sauce: https://httpd.apache.org/docs/2.4/howto/auth.html
